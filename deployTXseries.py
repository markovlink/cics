#!/usr/bin/python
import os.path
import urllib2
import subprocess
import shutil
import sys

class BaseOS(object):
    def __init__(self):
        print("BaseOS constructor")

    def install_prereqs(self):
        raise NotImplementedError("Subclass should implement this abstract method.")

    def is_supported(self):
        raise NotImplementedError("Subclass should implement this abstract method.")

    def call_command(self, args):
        p = subprocess.Popen(args, shell=False, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        for line in p.stdout.readlines():
            sys.stdout.write(line)
        retval = p.wait()

    def os_factory():
        if os.path.isfile("/etc/os-release"):
            if 'Ubuntu' in open('/etc/os-release').read():
                return UbuntuOS()
        elif os.path.isfile("/etc/redhat-release"):
            return RhelOS()
        else:
            print("Error. This is an unsupported flavour or operating system.")
            return OtherOS()
    os_factory = staticmethod(os_factory)

    def download_file(self, url, destination):
        headers = { "X-Auth-User":"IBMOS317316-2:txseriestrial","X-Auth-Key":"cfbd815f890740034c2110889c084a0e504fe7a970ebd6b367a2f099a8e6c6a5"}
        auth_url = 'https://sng01.objectstorage.softlayer.net/auth/v1.0/'
        request = urllib2.Request(auth_url)
        for key,value in headers.items():
            request.add_header(key,value)

        response = urllib2.urlopen(request)
        for name in response.info().headers:
            if name.find("X-Auth-Token:") != -1:
                auth=name
        auth_value=auth.strip().split(":")[1].strip()

        request = urllib2.Request(url)
        headers = {"X-Auth-Token": auth_value}
        for key,value in headers.items():
            request.add_header(key,value)
        response = urllib2.urlopen(request)
        print response.info().headers

        new_file = open(destination, "w")
        new_file.write(response.read())
        new_file.close()

    def download_install_txseries(self):
        url = 'https://sng01.objectstorage.softlayer.net/v1/AUTH_3401d304-5d9b-46f4-a3db-f85c9c7352fd/TXSeriesTrialVersion/TXSeriesV82-Evaluation-Linux.bin'
        destination = "/tmp/TXSeries82-Trial-Linux.bin"
        self.download_file(url,destination)

        url = 'https://sng01.objectstorage.softlayer.net/v1/AUTH_3401d304-5d9b-46f4-a3db-f85c9c7352fd/TXSeriesTrialVersion/TXSeries82-Trial-Linux.res'
        destination = "/tmp/TXSeries82-Trial-Linux.res"
        self.download_file(url,destination)

        self.call_command(["chmod", "777", "/tmp/TXSeries82-Trial-Linux.bin"])
        self.call_command(["/tmp/TXSeries82-Trial-Linux.bin", "-i", "silent", "-f" , "/tmp/TXSeries82-Trial-Linux.res"])


    def configure_txseries(self):
        self.call_command(["ln", "-s", "/opt/ibm/cics/etc/3270keys.xterm", "/etc/3270.keys"])
        self.call_command(["su", "-", "root", "-c", "cicscp -v create region IVPREG DefaultFileServer=/.:/cics/sfs/IVPSFS"])
        self.call_command(["su", "-", "root", "-c", "cicsivp -r IVPREG -s /.:/cics/sfs/IVPSFS -v sfs_SIVPSFS"])
        self.call_command(["su", "-", "root", "-c", "cicscp -v start region IVPREG StartType=cold"])

    def update_motd_progress(self):
        if os.path.isfile(self.motd_file):
            shutil.copyfile(self.motd_file, self.motd_file_backup)
        else:
            open(self.motd_file_backup,"a").close()
        file = open(self.motd_file, "a")
        file.write("*******************************************************************************\n")
        file.write("*                                                                             *\n")
        file.write("*                 Welcome to TXSeries V8.2 Evaluation Edition                 *\n")
        file.write("*                 -------------------------------------------                 *\n")
        file.write("*                                                                             *\n")
        file.write("*  Provisioning of TXSeries is now in progress...                             *\n")
        file.write("*                                                                             *\n")
        file.write("*  TXSeries configuration usually takes around 20 minutes after the           *\n")
        file.write("*  operating system is provisioned.                                           *\n")
        file.write("*                                                                             *\n")
        file.write("*  You need to logout and login for TXSeries specific environment to be       *\n")
        file.write("*  updated.                                                                   *\n")
        file.write("*                                                                             *\n")
        file.write("*  Please re-login after sometime.                                            *\n")
        file.write("*                                                                             *\n")
        file.write("*******************************************************************************\n")
        file.close()

    def update_motd_success(self):
        if os.path.isfile(self.motd_file_backup):
            shutil.copyfile(self.motd_file_backup, self.motd_file)
        file = open(self.motd_file, "a")
        file.write("*******************************************************************************\n")
        file.write("*                 Welcome to TXSeries V8.2 Evaluation Edition                 *\n")
        file.write("*                 -------------------------------------------                 *\n")
        file.write("*                                                                             *\n")
        file.write("*  Provisioning of TXSeries V8.2 evaluation edition is now COMPLETE.          *\n")
        file.write("*                                                                             *\n")
        file.write("*  Getting started with TXSeries V8.2:                                        *\n")
        file.write("*  1) Connect to the configured region(TXSeries instance), IVPREG:            *\n")
        file.write("*        - Run command 'cicslterm -r IVPREG'                                  *\n")
        file.write("*        - Type MENU as the transaction to enter the sample's menu            *\n")
        file.write("*  2) TXSeries administration console:                                        *\n")
        file.write("*       Logon to https://<ip>:9443/txseries/admin                              *\n")
        file.write("*  For more information:                                                      *\n")
        file.write("*  a) TXSeries forum:                                                         *\n")
        file.write("*       'https://www.ibm.com/developerworks/community/forums/html/            *\n")
        file.write("*       forum?id=11111111-0000-0000-0000-000000001014'                        *\n")
        file.write("*  b) TXSeries blog:                                                          *\n")
        file.write("*       'https://www.ibm.com/developerworks/community/blogs/                  *\n")
        file.write("*        DoMoreWithTXSeries'                                                  *\n")
        file.write("*******************************************************************************\n")
        file.close()

    def update_motd_fail(self):
        if os.path.isfile(self.motd_file_backup):
            shutil.copyfile(self.motd_file_backup, self.motd_file)
        file = open(self.motd_file, "a")
        file.write("*******************************************************************************\n")
        file.write("*                                                                             *\n")
        file.write("*                 Welcome to TXSeries V8.2 Evaluation Edition                 *\n")
        file.write("*                 -------------------------------------------                 *\n")
        file.write("*                                                                             *\n")
        file.write("*  Provisioning of TXSeries V8.2 evaluation edition failed.                   *\n")
        file.write("*                                                                             *\n")
        file.write("*  Check the provisioning logs located in /tmp/txseries_deploy.log for more   *\n")
        file.write("*  details.                                                                   *\n")
        file.write("*                                                                             *\n")
        file.write("*  Post any question on the TXSeries forum:                                   *\n")
        file.write("*       'https://www.ibm.com/developerworks/community/forums/html/            *\n")
        file.write("*        forum?id=11111111-0000-0000-0000-000000001014'                       *\n")
        file.write("*                                                                             *\n")
        file.write("*******************************************************************************\n")
        file.close()

class UbuntuOS(BaseOS):
    def __init__(self):
        print("Ubuntu flavour")
        self.motd_file="/etc/motd"
        self.motd_file_backup="/etc/motd.tail_txseries_backup"

    def install_prereqs(self):
        self.install_package("pdksh")
        self.install_package("libpam0g:i386")
        self.install_package("libpam-modules:i386")
        self.install_package("libc6-dev:i386")
        self.install_package("libstdc++6:i386")
        self.install_package("libncurses5:i386")
        self.install_package("gawk")
        self.install_package("xinetd")
        self.call_command(["ln", "-s", "/usr/bin/awk", "/bin/awk"])

#    def configure_txseries(self):
#        self.call_command(["ln", "-s", "/opt/ibm/cics/etc/3270keys.xterm", "/etc/3270.keys"])
#        self.call_command(["su", "-", "root", "-c", "cicscp -v create region IVPREG DefaultFileServer=/.:/cics/sfs/IVPSFS"])
#        self.call_command(["su", "-", "root", "-c", "cicscp -v start region IVPREG StartType=cold"])

    def install_package(self, packageName):
        self.call_command(["apt-get","install",packageName, "-y"])

    def is_supported(self):
        return True

class RhelOS(BaseOS):
    def __init__(self):
        print("Linux, RHEL like flavour")
        self.motd_file="/etc/motd"
        self.motd_file_backup="/etc/motd_txseries_backup"

    def install_package(self, packageName):
        self.call_command(["yum","install",packageName, "-y"])

    def install_prereqs(self):
        self.install_package("glibc.i686")
        self.install_package("libgcc.i686")
        self.install_package("glibc-devel.i686")
        self.install_package("ksh")
        self.install_package("libstdc++.i686")
        self.install_package("ncurses-libs.i686")
        self.install_package("pam.i686")

    def is_supported(self):
        return True


class OtherOS(BaseOS):
    def __init__(self):
        print("Unsupported OS")
        self.motd_file="/etc/motd"
        self.motd_file_backup="/etc/motd_txseries_backup"

    def is_supported(self):
        return False


#main
currentOS = RhelOS()
currentOS.update_motd_progress()
if currentOS.is_supported == False:
    print("Exiting. This is not a supported operating system.")
    currentOS.update_motd_fail()
    exit(1)
currentOS.install_prereqs()
currentOS.download_install_txseries()
currentOS.configure_txseries()
currentOS.update_motd_success()