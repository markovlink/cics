# LICENSE GPL 2.0
#
# Copyright (c) 2015 Oracle and/or its affiliates. All rights reserved.
#fedora with tsinghua repo and update
FROM docker.io/pilchard/fedora:latest

MAINTAINER pilchard <ludundun@126.com>

ADD deployTXseries.py /tmp

RUN dnf -y install python perl net-tools procps-ng gcc gdb bzip2 &&\
    python /tmp/deployTXseries.py &&\
    dnf clean all
    
USER cics

CMD ["bash"]
